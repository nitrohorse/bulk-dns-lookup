# Bulk DNS Lookup

Python script for seeing how effective a resolver's filtering is against a blocklist. Uses [doggo](https://github.com/mr-karan/doggo/) under the hood.

## Requirements
* [Doggo](https://github.com/mr-karan/doggo/#installation)
```
pip3 install tabulate Halo
```

## Usage
```
python3 ./dogs.py domains-*.txt resolvers.txt
```
* `domains.txt`:
	* Newline-separated list of domains to resolve.
	* Script does _not_ support Hosts format, e.g. `127.1 <domain>`
	* More domains-based lists can be found at:
		* [Personal collection](https://gitlab.com/-/snippets/2083166)
		* https://firebog.net/
		* https://github.com/nextdns/metadata
		* https://filterlists.com/
* `resolvers.txt`:
	* Newline-separate list of resolvers to test.
	* Doggo supports [multiple transport protocols](https://github.com/mr-karan/doggo/#features)!
	* More resolvers can be found at:
		* https://encrypted-dns.party/
		* https://github.com/curl/curl/wiki/DNS-over-HTTPS

## Example Output

**domains-mixed.txt**
```terminal
resolver                                          percentageBlocked      unresolvedDomains    totalDomains
------------------------------------------------  -------------------  -------------------  --------------
https://dns.hostux.net/ads                        78%                                  336             433
https://doh1.blahdns.com/dns-query                77%                                  332             433
https://freedns.controld.com/p2                   74%                                  321             433
https://dns-nyc.aaflalo.me/dns-query              73%                                  317             433
https://doh.tiar.app/dns-query                    69%                                  298             433
https://doh.la.ahadns.net/dns-query               69%                                  297             433
https://dns.nextdns.io/4dd2ac                     68%                                  293             433
tls://fi.dot.dns.snopyta.org                      60%                                  259             433
https://adblock.mydns.network/dns-query           59%                                  257             433
https://adfree.usableprivacy.net/                 58%                                  251             433
https://doh.libredns.gr/ads                       57%                                  247             433
tls://fdns1.dismail.de                            57%                                  247             433
https://dns-weblock.wevpn.com/dns-query           56%                                  242             433
tls://dns-family.adguard.com                      49%                                  214             433
tls://dns.adguard.com                             49%                                  213             433
https://doh.cleanbrowsing.org/doh/family-filter/  44%                                  192             433
https://doh.cleanbrowsing.org/doh/adult-filter/   44%                                  191             433
tls://dns.quad9.net                               44%                                  189             433
https://dns.blokada.org/dns-query                 43%                                  187             433
tls://family.cloudflare-dns.com                   43%                                  186             433
tls://security-filter-dns.cleanbrowsing.org       42%                                  183             433
tls://security.cloudflare-dns.com                 42%                                  180             433
https://dns.twnic.tw/dns-query                    40%                                  174             433
https://doh.ffmuc.net/dns-query                   36%                                  156             433
tls://dns2.digitalcourage.de                      36%                                  154             433
https://anycast.uncensoreddns.org/dns-query       35%                                  153             433
https://firefox.dns.nextdns.io                    35%                                  153             433
https://ordns.he.net/dns-query                    35%                                  153             433
tls://one.one.one.one                             35%                                  153             433
https://dns.nextdns.io                            35%                                  152             433
https://doh.42l.fr/dns-query                      35%                                  152             433
https://resolver-eu.lelux.fi/dns-query            35%                                  152             433
https://zero.rethinkdns.com/dns-query             35%                                  152             433
tls://dns.switch.ch                               35%                                  151             433
https://doh.opendns.com/dns-query                 34%                                  148             433
https://doh.familyshield.opendns.com/dns-query    34%                                  146             433
tls://protected.canadianshield.cira.ca            27%                                  119             433
tls://family.canadianshield.cira.ca               27%                                  116             433
```

**domains-ads.txt**
```terminal
resolver                                          percentageBlocked      unresolvedDomains    totalDomains
------------------------------------------------  -------------------  -------------------  --------------
https://freedns.controld.com/p2                   99%                                  223             225
https://dns.hostux.net/ads                        98%                                  221             225
https://dns-nyc.aaflalo.me/dns-query              94%                                  212             225
https://doh.tiar.app/dns-query                    94%                                  211             225
https://doh1.blahdns.com/dns-query                89%                                  201             225
https://adblock.mydns.network/dns-query           84%                                  189             225
https://doh.la.ahadns.net/dns-query               84%                                  189             225
https://adfree.usableprivacy.net/                 82%                                  185             225
https://dns.nextdns.io/4dd2ac                     82%                                  185             225
https://doh.libredns.gr/ads                       82%                                  185             225
tls://fdns1.dismail.de                            82%                                  185             225
https://us-east.adhole.org/dns-query              80%                                  179             225
tls://dns-family.adguard.com                      65%                                  146             225
tls://dns.adguard.com                             65%                                  146             225
https://dns-weblock.wevpn.com/dns-query           35%                                   78             225
https://dns.twnic.tw/dns-query                    10%                                   22             225
tls://dns.quad9.net                               10%                                   22             225
https://dns.blokada.org/dns-query                 8%                                    19             225
https://doh.cleanbrowsing.org/doh/family-filter/  8%                                    19             225
https://doh.familyshield.opendns.com/dns-query    8%                                    18             225
https://doh.cleanbrowsing.org/doh/adult-filter/   7%                                    16             225
tls://fi.dot.dns.snopyta.org                      7%                                    16             225
tls://security-filter-dns.cleanbrowsing.org       7%                                    16             225
https://anycast.uncensoreddns.org/dns-query       6%                                    14             225
https://doh.opendns.com/dns-query                 6%                                    14             225
https://doh.42l.fr/dns-query                      6%                                    13             225
https://resolver-eu.lelux.fi/dns-query            5%                                    12             225
https://doh.ffmuc.net/dns-query                   5%                                    11             225
tls://dns.switch.ch                               5%                                    11             225
tls://family.cloudflare-dns.com                   5%                                    11             225
tls://security.cloudflare-dns.com                 5%                                    11             225
https://dns.nextdns.io                            4%                                    10             225
https://firefox.dns.nextdns.io                    4%                                    10             225
https://ordns.he.net/dns-query                    4%                                    10             225
https://zero.rethinkdns.com/dns-query             4%                                    10             225
tls://one.one.one.one                             4%                                    10             225
tls://dns2.digitalcourage.de                      4%                                    10             225
tls://family.canadianshield.cira.ca               4%                                    10             225
tls://protected.canadianshield.cira.ca            4%                                    10             225
```