# Example usage:
# $ python ./dogs.py domains.txt resolvers.txt

import sys
import subprocess
import json
import tabulate
import datetime
import time
from halo import Halo

if len(sys.argv) != 3:
	raise ValueError('Please provide the domain and resolver list path arguments.')

domainsPath = sys.argv[1]
resolversPath = sys.argv[2]

domainsFile = open(domainsPath, 'r')
domains = domainsFile.readlines()

resolversFile = open(resolversPath, 'r')
resolvers = resolversFile.readlines()

totalDomains = 0
resolvedDomains = 0
unresolvedDomains = 0

startTimer = 0
endTimer = 0

dataset = []

for index, resolver in enumerate(resolvers, start=1):
	if (resolver[-1] == '\n'):
		resolver = resolver[:-1]

	spinner = Halo(text='Resolving domains with ' + resolver + ' (' + str(index) + '/' + str(len(resolvers)) + ')', spinner='dots')
	spinner.start()

	startTimer = datetime.datetime.now().replace(microsecond=0)

	totalDomains = 0
	resolvedDomains = 0
	unresolvedDomains = 0

	for domain in domains:
		if (domain == '\n' or domain[0] == '#'):
			continue

		totalDomains += 1

		if (domain[-1] == '\n'):
			domain = domain[:-1]

		try:
			res = subprocess.run(args = ['doggo', domain, '@' + resolver, '--json', '--timeout=3', '--search=false'],
								stdout = subprocess.PIPE,
								stderr=subprocess.DEVNULL)
		except:
			unresolvedDomains += 1
			continue

		data = json.loads(res.stdout)
		if data[0]['answers'] is None or \
			data[0]['answers'][0]['address'] == '0.0.0.0' or \
			data[0]['answers'][0]['address'] == '127.0.0.1':
			unresolvedDomains += 1
		else:
			resolvedDomains += 1

	# add result to collection
	result = {}
	result['resolver'] = resolver
	result['percentageBlocked'] = str(round((unresolvedDomains / totalDomains) * 100.0)) + '%'
	result['unresolvedDomains'] = unresolvedDomains
	result['totalDomains'] = totalDomains

	dataset.append(result)

	endTimer = datetime.datetime.now().replace(microsecond=0)
	spinner.stop_and_persist(text=str(endTimer - startTimer) + ' | ' + resolver, symbol='✅')

# sort list by highest block percentage
dataset = sorted(dataset, key=lambda k: k['unresolvedDomains'], reverse=True)

header = dataset[0].keys()
rows = [x.values() for x in dataset]

print()
print(tabulate.tabulate(rows, header))

